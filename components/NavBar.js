import Link from 'next/link';
import { Navbar, Nav } from 'react-bootstrap';

export default function NavBar(){
	return(

		<Navbar expand="lg" className="bg-primary fixed-top">
			<Navbar.Brand href="/">Joy Pague</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Link href="/about">
						<a className="nav-link text-light text-center">
							About Me
						</a>
					</Link>
					<Link href="/project">
						<a className="nav-link text-light text-center">
							Project
						</a>
					</Link>
					<Link href="/education">
						<a className="nav-link text-light text-center">
							Education
						</a>
					</Link>
					<Link href="/work">
						<a className="nav-link text-light text-center">
							Work Experience
						</a>
					</Link>
					<Link href="/contact">
						<a className="nav-link text-light text-center">
							Contact
						</a>
					</Link>
				</Nav>
			</Navbar.Collapse>
		</Navbar>

		)
}