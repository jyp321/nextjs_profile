import React from 'react';
import NavBar from '../components/NavBar';
import Footer from '../components/Footer';
import 'bootstrap/dist/css/bootstrap.min.css';
import styles from '../styles/Home.module.css'
import '../styles/globals.css'


function MyApp({ Component, pageProps }) {

  return(

  		<>
	  		<NavBar />
	   		<Component {...pageProps} />
	   		<Footer />
   		</>
  	) 
}

export default MyApp
