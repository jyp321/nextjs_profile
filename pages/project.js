import React from 'react';
import Head from 'next/head';
import {Container, Row, Card, Col} from 'react-bootstrap'
import Projects from '../data/projects'

export default function Project(){
	const projectsData = Projects.map((project)=>{
		return(
		<Col>
			<Card style={{width: "18rem"}} className="m-3">
				<Card.Img variant= "top" src={project.src}/>
				<Card.Body>
					<Card.Title>{project.name}</Card.Title>
					<Card.Text>{project.description}</Card.Text>
					<Card.Text><a href={project.url}>link here</a></Card.Text>
				</Card.Body>
			</Card>
		</Col>
		)
	})

	return(

		<>
			<Container className="my-5 text-center">
				<h1>Projects</h1>
				<Row className="justify-content">{projectsData}</Row>
			</Container>
		</>

		)
	
}