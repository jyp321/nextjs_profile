import React from 'react';
import Head from 'next/head';
import { Container, Row, Card } from 'react-bootstrap';
import Skills from '../data/skills';



export default function About(){
	const skillsData = Skills.map((skill) => {
		return (
			<Card style={{width: "18rem"}} className="m-3">
				<Card.Img variant= "top" src={skill.src}/>
				<Card.Body>
					<Card.Title>{skill.name}</Card.Title>
				</Card.Body>
			</Card>
		)	
	})

	return(

		<>
			<Container className="my-5 text-center">
				<div>
				
				<h1>About Me</h1>
					<p>
						I am a full stack web developer and an instructor. I am passionate about teaching and molding students to become better in the future. I also love programming.
					</p>
					<p>
						I wanted to reach my maximum potential in coding in order to be effective and efficient in any campaign I will pursue.
					</p>
				</div>

				<Row>{skillsData}</Row>	

			</Container>




		</>

		)
}