import React from 'react'; 
import {Container, Image} from 'react-bootstrap';

export default function Home(){
	return(


		<Container className="my-5 text-center">
			<Image src="/c1.jpg" className="img-fluid" thumbnail  />
			<h1>Joy Pague</h1>
			<h4>Full Stack Web Developer</h4>
		</Container>


		)
}